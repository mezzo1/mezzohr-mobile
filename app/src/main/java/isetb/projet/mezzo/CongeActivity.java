package isetb.projet.mezzo;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.List;

import isetb.projet.mezzo.Adapter.CongeAdapter;
import isetb.projet.mezzo.Model.Conge;
import isetb.projet.mezzo.Utils.Apis;
import isetb.projet.mezzo.Utils.CongeService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CongeActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    List<Conge> congeList;
    Button btnajout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conge);

        btnajout=findViewById(R.id.btnajout);
        btnajout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(CongeActivity.this, AjoutercongeActivity.class));
            }
        });
        recyclerView=findViewById(R.id.recyclerconge);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        CongeService apiService= Apis.getCongeService();

        Call<List<Conge>> call=apiService.getConge();
        call.enqueue(new Callback<List<Conge>>() {
            @Override
            public void onResponse(Call<List<Conge>> call, Response<List<Conge>> response) {
                congeList=response.body();
                CongeAdapter c=new CongeAdapter(congeList);
                recyclerView.setAdapter(c);
                recyclerView.smoothScrollToPosition(0);
            }

            @Override
            public void onFailure(Call<List<Conge>> call, Throwable t) {
                Toast.makeText(CongeActivity.this,t.toString(),Toast.LENGTH_LONG).show();
                Log.i("AppUser",t.toString());
            }
        });


    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_options,menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){

            case R.id.logout:{
                Intent i=new Intent(CongeActivity.this,LoginActivity.class);
                startActivity(i);
            }

            case R.id.dashboard:{
                Intent i=new Intent(CongeActivity.this,dashboardRHActivity.class);
                startActivity(i);
            }
            case R.id.Profile:{
                Intent i=new Intent(CongeActivity.this,ProfileActivity.class);
                startActivity(i);
            }




        }
        return super.onOptionsItemSelected(item);
    }
}
