package isetb.projet.mezzo.Utils;

import java.util.List;

import isetb.projet.mezzo.Model.Conge;
import isetb.projet.mezzo.Model.CongeRequest;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface CongeService {
    @GET("/Conge")
    Call<List<Conge>> getConge();
    @PUT("/Conge/{i}")
    Call<Conge> updateConge( @Path("i") Long id, @Body Conge conge);
    @DELETE("/Conge/{i}")
    Call<Void> deleteConge(@Path("i") Long id);
    @POST("/Conge")
    Call<CongeRequest> createConge(@Body CongeRequest congeRequest);

}
