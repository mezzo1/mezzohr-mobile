package isetb.projet.mezzo.Utils;

import java.util.List;

import isetb.projet.mezzo.Model.LoginRequest;
import isetb.projet.mezzo.Model.User;
import isetb.projet.mezzo.Model.UserRequest;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface UserService {

    @GET("/user")
    Call<List<User>> getusers();
    @PUT("/user")
    Call<User> updateuser(@Body User user);
    @DELETE("/user/{i}")
    Call<Void> deleteuser(@Path("i") Long id);
    @POST("/user")
    Call<User> createuser(@Body UserRequest user);

    @POST("/user/findbyep")
    Call<User> finduser(@Body LoginRequest loginRequest);
    @GET("/user/{i}")
    Call<User> findUserById(@Path("i") Long id);
}
