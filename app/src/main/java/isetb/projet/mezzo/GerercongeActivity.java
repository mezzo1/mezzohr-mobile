package isetb.projet.mezzo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import isetb.projet.mezzo.Model.Conge;
import isetb.projet.mezzo.Utils.Apis;
import isetb.projet.mezzo.Utils.CongeService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GerercongeActivity extends AppCompatActivity {
 Button btnacc ;
 Button btnref;
 EditText e1,e2,e3,e4,e5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gererconge);

        e1 = findViewById(R.id.id);
        e2 = findViewById(R.id.DD);
        e3 = findViewById(R.id.DF);
        e4 = findViewById(R.id.TC);
        e5=findViewById(R.id.status);
        btnacc = findViewById(R.id.btnacc);
        btnref = findViewById(R.id.btnref);

        Bundle extras = getIntent().getExtras();
        final String id = String.valueOf(extras.getLong("id"));
        e1.setText(id);
        final String DD = String.valueOf(extras.getString("DD"));
        e2.setText(DD);
        final String DF = String.valueOf(extras.getString("DF"));
        e3.setText(DF);
        final String TC = extras.getString("TC");
        e4.setText(TC);
        final  String Status =String.valueOf(extras.getString("S"));
        e5.setText(Status);
        btnref.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CongeService apiService = Apis.getCongeService();
                Long id= Long.valueOf(e1.getText().toString());
                String DD=e2.getText().toString();
                String DF =e3.getText().toString();
                String TC=e4.getText().toString();


                Conge conge=new Conge(id,DD,DF,TC,"false");
                Call<Conge> call=apiService.updateConge(id,conge);
                call.enqueue(new Callback<Conge>() {
                    @Override
                    public void onResponse(Call<Conge> call, Response<Conge> response) {
                        if (response.isSuccessful()){
                            Toast t=  Toast.makeText(GerercongeActivity.this,"conge refused!!",Toast.LENGTH_SHORT);
                            t.show();
                            Intent i=new Intent(GerercongeActivity.this,CongeActivity.class);
                            startActivity(i);
                        }

                    }

                    @Override
                    public void onFailure(Call<Conge> call, Throwable t) {
                        Log.i("AppUser",t.toString());
                    }
                });


            }
        });
        btnacc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CongeService apiService = Apis.getCongeService();
                Long id= Long.valueOf(e1.getText().toString());
                String DD=e2.getText().toString();
                String DF =e3.getText().toString();
                String TC=e4.getText().toString();


                Conge conge=new Conge(id,DD,DF,TC,"true");
                Call<Conge> call=apiService.updateConge(id,conge);
                call.enqueue(new Callback<Conge>() {
                    @Override
                    public void onResponse(Call<Conge> call, Response<Conge> response) {
                        if (response.isSuccessful()){
                            Toast t=  Toast.makeText(GerercongeActivity.this,"conge accepted!!",Toast.LENGTH_SHORT);
                            t.show();
                            Intent i=new Intent(GerercongeActivity.this,CongeActivity.class);
                            startActivity(i);
                        }

                    }

                    @Override
                    public void onFailure(Call<Conge> call, Throwable t) {
                        Log.i("AppUser",t.toString());
                    }
                });


            }
        });

    }
    }

