package isetb.projet.mezzo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import isetb.projet.mezzo.Model.LoginRequest;
import isetb.projet.mezzo.Model.User;
import isetb.projet.mezzo.Utils.Apis;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    EditText email , password;
    Button Login,btnlink ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();


        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        Login = findViewById(R.id.login);
        btnlink = findViewById(R.id.btnsignup);
        btnlink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this,inscription.class));

            }
        });
        Login.setOnClickListener(view -> {

            if(TextUtils.isEmpty(email.getText().toString()) || TextUtils.isEmpty(password.getText().toString())){
                Toast.makeText(LoginActivity.this,"Username / Password Required", Toast.LENGTH_LONG).show();
            }else{
                //proceed to login
                login();
            }

        });
    }

    public void login(){
        LoginRequest loginRequest = new LoginRequest(email.getText().toString(),password.getText().toString());

        Call<User> loginResponseCall = Apis.getUserService().finduser(loginRequest);
        loginResponseCall.enqueue(new Callback<User>() {
            @Override
            public void onResponse(@NonNull Call<User> call, @NonNull Response<User> response) {

                if(response.isSuccessful()){
                    Toast.makeText(LoginActivity.this,"Login Successful", Toast.LENGTH_LONG).show();
                    User loginResponse = response.body();

                   Long id= response.body().getId();

                    new Handler().postDelayed(() -> {
                        assert loginResponse != null;
                        Intent I = new Intent (LoginActivity.this, ProfileActivity.class);
                        I. putExtra("id", id);
                        startActivity (I) ;

                        // startActivity(new Intent(LoginActivity.this,dashboardRHActivity.class));

                    },700);

                }else{
                    Toast.makeText(LoginActivity.this,"Login Failed", Toast.LENGTH_LONG).show();

                }

            }

            @Override
            public void onFailure(@NonNull Call<User> call, @NonNull Throwable t) {
                Toast.makeText(LoginActivity.this,"Throwable "+t.getLocalizedMessage(), Toast.LENGTH_LONG).show();

            }
        });



    }
}