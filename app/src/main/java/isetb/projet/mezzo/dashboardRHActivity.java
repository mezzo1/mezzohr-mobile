package isetb.projet.mezzo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.List;

import isetb.projet.mezzo.Adapter.UserAdapter;
import isetb.projet.mezzo.Model.User;
import isetb.projet.mezzo.Utils.Apis;
import isetb.projet.mezzo.Utils.UserService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class dashboardRHActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    List<User> userList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_rh);
        recyclerView=findViewById(R.id.recycler);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        UserService apiService= Apis.getUserService();

        Call<List<User>> call=apiService.getusers();
        call.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {

                userList=response.body();
                UserAdapter a=new UserAdapter(userList);
                recyclerView.setAdapter(a);
                recyclerView.smoothScrollToPosition(0);


            }


            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                Toast.makeText(dashboardRHActivity.this,t.toString(),Toast.LENGTH_LONG).show();
                Log.i("AppUser",t.toString());
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_options,menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){

            case R.id.logout:{
                Intent i=new Intent(dashboardRHActivity.this,LoginActivity.class);
                startActivity(i);
            }
            case R.id.conge:{
                Intent i=new Intent(dashboardRHActivity.this,CongeActivity.class);
                startActivity(i);
            }
            case R.id.Profile:{
                Intent i=new Intent(dashboardRHActivity.this,ProfileActivity.class);
                startActivity(i);
            }




        }
        return super.onOptionsItemSelected(item);
    }

}
