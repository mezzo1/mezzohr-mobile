package isetb.projet.mezzo;

public class PhotoUser {
    private int id;
    private byte[] image;

    public PhotoUser() {
    }
    public PhotoUser( byte[] image) {

        this.image = image;
    }

    public PhotoUser(int id ,byte[] image) {
        this.id = id;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
}