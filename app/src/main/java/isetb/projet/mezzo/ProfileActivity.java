package isetb.projet.mezzo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import isetb.projet.mezzo.Model.User;
import isetb.projet.mezzo.Utils.Apis;
import isetb.projet.mezzo.Utils.UserService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends AppCompatActivity {
    User user;
    TextView t2,t3,t4,t5,t6;
    ImageButton edit;
    UserPhotoDataBase db;
    ImageView img ,changePhoto;
    PhotoUser Photo ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
       // listPhoto=new ArrayList<PhotoUser>();
        db= new UserPhotoDataBase(this);


        t2=findViewById(R.id.FN);
        t3=findViewById(R.id.LN);
        t4=findViewById(R.id.email);
        t5=findViewById(R.id.dept);
        t6=findViewById(R.id.role);
        edit=findViewById(R.id.update);
        img=findViewById(R.id.photoo);
        changePhoto=findViewById(R.id.changephoto);

    ///////////////recuperation de l id
        UserService apiService= Apis.getUserService();
        Bundle extra = this.getIntent().getExtras();
        Long id = extra.getLong("id");

        Call<User> call=apiService.findUserById(id);
        Photo = db.getPhoto(Math.toIntExact(id));
        if ( Photo!=null){
                   Bitmap imgBitmap= BitmapFactory.decodeByteArray(Photo.getImage(),0,Photo.getImage().length);
                   img.setImageBitmap(imgBitmap);
                changePhoto.setImageResource(0);

            }

        else {
            changePhoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i=new Intent(ProfileActivity.this,addPhoto.class);
                    i. putExtra("id", id);
                    startActivity(i);
                }
            });
        //}
            }

        /////////////////
        call.enqueue(new Callback<User>() {

            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                user=response.body();
                t2.setText(user.getFirstname());
                t3.setText(user.getLastname());
                t4.setText(user.getEmail());
                t5.setText(user.getDept());
                t6.setText(user.getRole());
            }
            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(ProfileActivity.this,t.toString(),Toast.LENGTH_LONG).show();
                Log.i("AppUser",t.toString());
            }



        });
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ProfileActivity.this, UpdateActivity.class);
                i.putExtra("id", Long.valueOf(user.getId()));
                i.putExtra("firstname", String.valueOf(user.getFirstname()));
                i.putExtra("lastname", String.valueOf(user.getLastname()));
                i.putExtra("email", String.valueOf(user.getEmail()));
                i.putExtra("role", String.valueOf(user.getRole()));
                i.putExtra("dept", String.valueOf(user.getDept()));
                startActivity(i);

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_options,menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){

            case R.id.logout:{
                Intent i=new Intent(ProfileActivity.this,LoginActivity.class);
                startActivity(i);
            }

            case R.id.dashboard:{
                Intent i=new Intent(ProfileActivity.this,dashboardRHActivity.class);
                startActivity(i);
            }
            case R.id.conge:{
                Intent i=new Intent(ProfileActivity.this,CongeActivity.class);
                startActivity(i);
            }
            case R.id.Profile:{
            }




        }
        return super.onOptionsItemSelected(item);
    }

}