package isetb.projet.mezzo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import isetb.projet.mezzo.Model.Conge;
import isetb.projet.mezzo.Model.CongeRequest;
import isetb.projet.mezzo.Model.User;
import isetb.projet.mezzo.Model.UserRequest;
import isetb.projet.mezzo.Utils.Apis;
import isetb.projet.mezzo.Utils.CongeService;
import isetb.projet.mezzo.Utils.UserService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AjoutercongeActivity extends AppCompatActivity {
EditText T;
Button btndemander;
DatePicker Datede,Datefn;
User user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajouterconge);
        getSupportActionBar().hide();

         //Date_debut
        Datede = (DatePicker) findViewById(R.id.Datede);
        int   day  = Datede.getDayOfMonth();
        int   month;
        month = Datede.getMonth();
        int   year = Datede.getYear();
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String formatedDate = sdf.format(calendar.getTime());

         //Date_fin
        Datefn = (DatePicker) findViewById(R.id.Datefn);
        //for a date value
        int   day1  = Datefn.getDayOfMonth();
        int   month1;
        month = Datede.getMonth();
        int   year1 = Datede.getYear();
        Calendar calendar1 = Calendar.getInstance();
        calendar.set(year, month, day);
        SimpleDateFormat sd = new SimpleDateFormat("dd-MM-yyyy");
        String formatedDate1 = sd.format(calendar.getTime());

        //Type_Conge
        T= findViewById(R.id.T);


        btndemander=findViewById(R.id.btndemander);
        btndemander.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Datede = (DatePicker) findViewById(R.id.Datede);
                int   day  = Datede.getDayOfMonth();
                int   month;
                month = Datede.getMonth();
                int   year = Datede.getYear();
                Calendar calendar = Calendar.getInstance();
                calendar.set(year, month, day);
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                String formatedDate = sdf.format(calendar.getTime());
                Datefn = (DatePicker) findViewById(R.id.Datefn);
                int   day1  = Datefn.getDayOfMonth();
                int   month1;
                month1 = Datefn.getMonth();
                int   year1 = Datefn.getYear();
                Calendar calendar1 = Calendar.getInstance();
                calendar1.set(year1, month1, day1);
                SimpleDateFormat sd = new SimpleDateFormat("dd-MM-yyyy");
                String formatedDate1 = sd.format(calendar.getTime());
                String Type_Conge=T.getText().toString();
                Call<User> user=Apis.getUserService().findUserById(Long.valueOf(3));
                user.enqueue(new Callback<User>() {

    private User user;

    @Override
    public void onResponse(Call<User> calls, Response<User> response) {
        this.user =response.body();
        CongeRequest congeRequest = new CongeRequest(formatedDate,formatedDate1,Type_Conge,user);

        CongeService apiService= Apis.getCongeService();
        Call<CongeRequest> call =apiService.createConge(congeRequest );
        call.enqueue(new Callback<CongeRequest>() {
            @Override
            public void onResponse(Call<CongeRequest> call, Response<CongeRequest> response) {


                Toast.makeText(AjoutercongeActivity.this,"Conge Added successfully!!",Toast.LENGTH_SHORT);
                Intent i=new Intent(AjoutercongeActivity.this,CongeActivity.class);
                startActivity(i);
            }





            @Override
            public void onFailure(Call<CongeRequest> call, Throwable t) {
                Log.i("AppUser",t.toString());
            }
        });
    }

    @Override
    public void onFailure(Call<User> call, Throwable t) {
        Toast.makeText(AjoutercongeActivity.this, "user", Toast.LENGTH_SHORT).show();
    }
});

            }
        });



    }
}