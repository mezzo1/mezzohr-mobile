package isetb.projet.mezzo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;


import java.text.SimpleDateFormat;
import java.util.Calendar;

import isetb.projet.mezzo.Model.User;
import isetb.projet.mezzo.Model.UserRequest;
import isetb.projet.mezzo.Utils.Apis;
import isetb.projet.mezzo.Utils.UserService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class inscription extends AppCompatActivity {

    Button btninscri , btnlink;
    EditText fn,ln,email,password,repass;
    Spinner spinnerDepartments,spinnerRole;
    DatePicker datePicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscription);
        getSupportActionBar().hide();

        spinnerDepartments=findViewById(R.id.spinner_departments);
        ArrayAdapter<CharSequence>adapter=ArrayAdapter.createFromResource(this, R.array.Department, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spinnerDepartments.setAdapter(adapter);

        spinnerRole=findViewById(R.id.spinner_role);
        ArrayAdapter<CharSequence>adapterR=ArrayAdapter.createFromResource(this, R.array.role, android.R.layout.simple_spinner_item);
        adapterR.setDropDownViewResource(android.R.layout.simple_spinner_item);
        spinnerRole.setAdapter(adapterR);

        // for the dropdown value
        String text = spinnerRole.getSelectedItem().toString();

        //get values
        fn = findViewById(R.id.firstname);
        ln = findViewById(R.id.lastname);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        repass = findViewById(R.id.repassword);

        datePicker = (DatePicker) findViewById(R.id.datePicker);
        //for a date value
        int   day  = datePicker.getDayOfMonth();
        int   month;
        month = datePicker.getMonth();
        int   year = datePicker.getYear();
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String formatedDate = sdf.format(calendar.getTime());

        btninscri = findViewById(R.id.signup);
        btninscri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String firstname= fn.getText().toString();
                String lastname= ln.getText().toString();
                String emails= email.getText().toString();
                String passwords= password.getText().toString();
                String repasswords= repass.getText().toString();

                String role = spinnerRole.getSelectedItem().toString();
                String department = spinnerDepartments.getSelectedItem().toString();

                int   day  = datePicker.getDayOfMonth();
                int   month;
                month = datePicker.getMonth();
                int   year = datePicker.getYear();
                Calendar calendar = Calendar.getInstance();
                calendar.set(year, month, day);
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                String date = sdf.format(calendar.getTime());

                UserRequest ur = new UserRequest(firstname,lastname,emails,passwords,role,department,null,date);
                if(passwords.equals(repasswords)){
                    UserService apiService= Apis.getUserService();
                    Call<User> call=apiService.createuser(ur);
                    call.enqueue(new Callback<User>() {
                        @Override
                        public void onResponse(Call<User> call, Response<User> response) {
                            if (response.isSuccessful()){
                                Toast.makeText(inscription.this,"user Added successfully!!",Toast.LENGTH_SHORT);
                                Intent i=new Intent(inscription.this,LoginActivity.class);
                                startActivity(i);
                            }
                        }
                        @Override
                        public void onFailure(Call<User> call, Throwable t) {
                            Log.i("AppUser",t.toString());
                        }
                    });
                }


            }
        });

        btnlink = findViewById(R.id.godash);
        btnlink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(inscription.this,LoginActivity.class);
                startActivity(i);
            }
        });


    }
}