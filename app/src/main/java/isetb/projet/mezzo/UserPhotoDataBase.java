package isetb.projet.mezzo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import java.util.ArrayList;

public class UserPhotoDataBase extends SQLiteOpenHelper {

    private SQLiteDatabase db;
    public static final int DATABASE_VERSION=1;
    public static final String  DATABASE_NAME="Photo_db";
    public static final String TabelPhoto="Photo";
    public static final String USER_ID="id";
    public static final String CIMAGE="image";
    public static final String CREATE_UserPhoto_TABLE="CREATE TABLE "+TabelPhoto+"("
            +USER_ID+" INTEGER PRIMARY KEY , "
            +CIMAGE +" BLOB) ";

    public UserPhotoDataBase(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_UserPhoto_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+TabelPhoto);
        onCreate(sqLiteDatabase);
    }
    public void updatePhoto(PhotoUser c, int id) {
        db=this.getWritableDatabase();
        ContentValues values =new ContentValues();
        values.put(CIMAGE,c.getImage());
        long x= db.update(TabelPhoto,values,USER_ID+"LIKE ?",new String[]{String.valueOf(id)});
    }
    public void removePhoto(int id) {
        db=this.getWritableDatabase();
        long x= db.delete(TabelPhoto,USER_ID+"?",new String[]{String.valueOf(id)});
    }
    public Boolean addPhoto(PhotoUser c) {
        db=this.getWritableDatabase();
        ContentValues values =new ContentValues();
        values.put(USER_ID,c.getId());
        values.put(CIMAGE,c.getImage());
        long x= db.insert(TabelPhoto,null,values);

        return (x!=-1);
    }
public PhotoUser getPhoto(int id){
    db = this.getReadableDatabase();

    Cursor c =db.query(TabelPhoto,new String[]{USER_ID,CIMAGE},USER_ID +"=?",new String[]{String.valueOf(id)},null,null,null,null);

    if(c!= null && c.moveToFirst()){

            PhotoUser photoUser=new PhotoUser(c.getBlob(1));
        return  photoUser;
        }
else return null;



  }

    public int getCount() {
        db = this.getReadableDatabase();

        Cursor c =db.query(TabelPhoto,null,null,null,null,null,null);
        return c.getCount();
    }
    public ArrayList<PhotoUser> get() {
        ArrayList<PhotoUser> list=new ArrayList<>();
        db=this.getWritableDatabase();
        Cursor c =db.query(TabelPhoto,null,null,null,null,null,null,null);
        if(c!=null && c.getCount()>0){
            c.moveToFirst();
            do{
                PhotoUser photoUser=new PhotoUser(c.getInt(0),c.getBlob(1));
                list.add(photoUser);
            }while (c.moveToNext());
            return  list;
        }
        else{return null;}


    }
}
