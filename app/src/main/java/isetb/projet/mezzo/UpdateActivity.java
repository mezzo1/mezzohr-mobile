package isetb.projet.mezzo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import isetb.projet.mezzo.Model.User;
import isetb.projet.mezzo.Utils.Apis;
import isetb.projet.mezzo.Utils.UserService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateActivity extends AppCompatActivity {
EditText e1,e2,e3,e4,e5;
Spinner departement , role;
Button update;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);
        update=findViewById(R.id.update);

        Bundle extras = getIntent().getExtras();
        e1=findViewById(R.id.id);

        final String id = String.valueOf(extras.getLong("id"));
        e1.setText(id);

        e2=findViewById(R.id.firstname);
        final String fn = extras.getString("firstname");
        e2.setText(fn);

        e3=findViewById(R.id.lastname);
        final String ln = extras.getString("lastname");
        e3.setText(ln);

        e4=findViewById(R.id.email);
        final String email = extras.getString("email");
        e4.setText(email);

        role=findViewById(R.id.role);
        final String r =extras.getString("role");
        role.setSelection(getIndex(role, r));

        departement=findViewById(R.id.dept);
        final String dept =extras.getString("dept");
         departement.setSelection(getIndex(departement, dept));

update.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        UserService apiService = Apis.getUserService();
       Long id= Long.valueOf(e1.getText().toString());
       String fn=e2.getText().toString();
       String ln =e3.getText().toString();
       String email=e4.getText().toString();
       String r =role.getSelectedItem().toString();
       String d=departement.getSelectedItem().toString();
        User u=new User(id,fn,ln,email,r,d);
        Call<User> call=apiService.updateuser(u);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()){
                    Toast t=  Toast.makeText(UpdateActivity.this,"user updated successfully!!",Toast.LENGTH_SHORT);
                    t.show();
                    Intent i=new Intent(UpdateActivity.this,dashboardRHActivity.class);
                    startActivity(i);
                }

            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.i("AppUser",t.toString());
            }
        });


    }
});



    }

    private int getIndex(Spinner s, String dept) {
        for (int i=0;i<s.getCount();i++){
            if (s.getItemAtPosition(i).toString().equalsIgnoreCase(dept)){
                return i;
            }
        }

        return 0;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.optionmenu2,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){

            case R.id.Dashboard:{
                Intent i=new Intent(UpdateActivity.this,dashboardRHActivity.class);
                startActivity(i);
            }
        }
        return super.onOptionsItemSelected(item);
    }
}