package isetb.projet.mezzo.Adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import isetb.projet.mezzo.GerercongeActivity;
import isetb.projet.mezzo.Model.Conge;
import isetb.projet.mezzo.R;

public class CongeAdapter extends RecyclerView.Adapter<CongeViewHolder> {

    List<Conge> list;
    public CongeAdapter(List<Conge> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public CongeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_list, parent, false);
        return new CongeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CongeViewHolder holder, int position) {
        Conge c = list.get(position);
        holder.bind(c);
        holder.consulter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), GerercongeActivity.class);
                i.putExtra("DD", String.valueOf(c.getDate_debut()));
                i.putExtra("DF", String.valueOf(c.getDate_fin()));
                i.putExtra("id", c.getId());
                i.putExtra("TC", c.getTypeconge());
                // i.putExtra("N", String.valueOf(c.getNotifier()));
                i.putExtra("S",String.valueOf(c.getStatus()));
                v.getContext().startActivity(i);

            }
        } );


    }


    @Override
    public int getItemCount() {
        return list.size();
    }
}
