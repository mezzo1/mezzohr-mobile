package isetb.projet.mezzo.Adapter;

import static java.lang.Integer.parseInt;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import isetb.projet.mezzo.Model.User;
import isetb.projet.mezzo.R;

public class UserViewHolder extends RecyclerView.ViewHolder {
    TextView t1,t2,t3,t4,t5,t6;
     ImageButton delete,edit;

    public UserViewHolder(@NonNull View itemView) {
        super(itemView);
        t1=itemView.findViewById(R.id.id);
        t2=itemView.findViewById(R.id.FN);
        t3=itemView.findViewById(R.id.LN);
        t4=itemView.findViewById(R.id.email);
        t5=itemView.findViewById(R.id.dept);
        t6=itemView.findViewById(R.id.role);
        delete=itemView.findViewById(R.id.del);
        edit=itemView.findViewById(R.id.update);

    }

    public void bind(User user){
        t1.setText(String.valueOf(user.getId()));
        t2.setText(user.getFirstname());
        t3.setText(user.getLastname());
        t4.setText(user.getEmail());
        t5.setText(user.getDept());
        t6.setText(user.getRole());

    }
}
