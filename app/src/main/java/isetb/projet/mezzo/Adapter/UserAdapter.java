package isetb.projet.mezzo.Adapter;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import isetb.projet.mezzo.Model.User;
import isetb.projet.mezzo.R;
import isetb.projet.mezzo.UpdateActivity;
import isetb.projet.mezzo.Utils.Apis;
import isetb.projet.mezzo.Utils.UserService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserAdapter extends RecyclerView.Adapter<UserViewHolder> {
    List<User> list;

    public UserAdapter(List<User> list) {

        this.list = list;
    }
    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_users,parent,false);
        return new UserViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder holder, @SuppressLint("RecyclerView") int position) {
        User u=list.get(position);
            holder.bind(u);

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder a =new AlertDialog.Builder(v.getContext());
                a.setTitle("Delete user");
                a.setMessage("Are you sure to delete the user");
                a.setPositiveButton("oui", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                       UserService apiService= Apis.getUserService();
                        Call<Void> call = apiService.deleteuser(u.getId());
                        call.enqueue(new Callback<Void>() {
                            @Override
                            public void onResponse(Call<Void> call, Response<Void> response) {
                                if(response.isSuccessful()){
                                   Toast t= Toast.makeText(v.getContext(), "User deleted successfully!", Toast.LENGTH_SHORT);
                                    t.show();
                                    list.remove(position);
                                    notifyDataSetChanged();
                                }

                            }

                            @Override
                            public void onFailure(Call<Void> call, Throwable t) {
                                Log.i("AppUser",t.toString());
                            }
                        });


                    }
                });
                a.setNegativeButton("Non",null);
                a.show();
            }
        });
        holder.edit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i=new Intent(v.getContext() , UpdateActivity.class);
                        i.putExtra("firstname", String.valueOf(u.getFirstname()));
                        i.putExtra("lastname", String.valueOf(u.getLastname()));
                        i.putExtra("id", Long.valueOf(u.getId()));
                        i.putExtra("email", String.valueOf(u.getEmail()));
                        i.putExtra("role", String.valueOf(u.getRole()));
                        i.putExtra("dept",String.valueOf(u.getDept()));
                        v.getContext().startActivity(i);

                    }
                }
        );
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
