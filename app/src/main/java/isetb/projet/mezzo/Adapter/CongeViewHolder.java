package isetb.projet.mezzo.Adapter;

import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import isetb.projet.mezzo.Model.Conge;
import isetb.projet.mezzo.R;

public class CongeViewHolder extends RecyclerView.ViewHolder {
    TextView t1, t2, t3, t4,t5,t6;
    ImageButton consulter;
    Button demander;
    public CongeViewHolder(@NonNull View itemView) {
        super(itemView);
        t1 = itemView.findViewById(R.id.idConge);
        t2 = itemView.findViewById(R.id.DD);
        t3 = itemView.findViewById(R.id.DF);
        t4 = itemView.findViewById(R.id.TC);
        t5= itemView.findViewById(R.id.S);
        consulter=itemView.findViewById(R.id.consulter);
        demander=itemView.findViewById(R.id.btndemander);
    }


    public void bind(Conge c) {
        t1.setText(String.valueOf(c.getId()));
        t2.setText(c.getDate_debut());
        t3.setText(c.getDate_fin());
        t4.setText(c.getTypeconge());
        t5.setText(c.getStatus());
    }
}
