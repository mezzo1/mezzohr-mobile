package isetb.projet.mezzo;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import isetb.projet.mezzo.R;

public class addPhoto extends AppCompatActivity {
    ImageView image;
    Button btn;
    Bitmap img;
    Long id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_photo);
        image=findViewById(R.id.add);
        btn=findViewById(R.id.btn_add);
        Bundle extra = this.getIntent().getExtras();
        id = extra.getLong("id");
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent();
                i.setType("image/*");
                i.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(i,1);
            }
        });

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserPhotoDataBase db;
                ArrayList<PhotoUser> listPhoto;
                ByteArrayOutputStream byteArrayOutputStream= new ByteArrayOutputStream();
                img.compress(Bitmap.CompressFormat.PNG,0,byteArrayOutputStream);
                byte[]bytesImage = byteArrayOutputStream.toByteArray();
                db= new UserPhotoDataBase(addPhoto.this);
                PhotoUser c = new PhotoUser(Math.toIntExact(id),bytesImage);
                    if (db.addPhoto(c)){
                        Intent i = new Intent(addPhoto.this,ProfileActivity.class);
                        startActivity(i);
                        Toast.makeText(addPhoto.this,"Photo ajouter",Toast.LENGTH_LONG).show();


                    }



            }
        });


    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==1 && resultCode==RESULT_OK) {
            Uri selectedImage= data.getData() ;
            if(selectedImage!=null){
                image.setImageURI(selectedImage);
                try {
                    img = MediaStore.Images.Media.getBitmap(getContentResolver(),selectedImage);
                    Log.e("a","success");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }
}