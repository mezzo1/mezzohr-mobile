package isetb.projet.mezzo.Model;

import retrofit2.Call;

public class Conge {


    private Long id;
    private String date_debut;
    private String date_fin ;
    private String typeconge ;
    private  String  status;
    private String notifier;

    public Conge(Long id, String date_debut, String date_fin, String typecong,String status,String notifier) {
        this.id = id;
        this.date_debut = date_debut;
        this.date_fin = date_fin;
        this.typeconge = typeconge;
        this.status = status;
        this.notifier=notifier;
    }
    public Conge(Long id, String date_debut, String date_fin, String typecong,String status) {
        this.id = id;
        this.date_debut = date_debut;
        this.date_fin = date_fin;
        this.typeconge = typeconge;
        this.status = status;

    }




    public Conge() {
    }




    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDate_debut() {
        return date_debut;
    }

    public void setDate_debut(String date_debut) {
        this.date_debut = date_debut;
    }

    public String getDate_fin() {
        return date_fin;
    }

    public void setDate_fin(String date_fin) {
        this.date_fin = date_fin;
    }

    public String getTypeconge() {
        return typeconge;
    }

    public void setTypeconge(String typeconge) {
        this.typeconge = typeconge;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNotifier() {
        return notifier;
    }

    public void setNotifier(String notifier) {
        this.notifier = notifier;
    }
}


