package isetb.projet.mezzo.Model;

import retrofit2.Call;

public class CongeRequest {
    private String date_debut;
    private String date_fin;
    private String typeconge;
    private Boolean notifier;
    private Boolean status;
    private User user;

    public CongeRequest(String date_debut, String date_fin, String typeconge, Boolean notifier, Boolean status){
        this.date_debut = date_debut;
        this.date_fin = date_fin;
        this.typeconge = typeconge;
        this.notifier = notifier;
        this.status = status;

    }
    public CongeRequest( String date_debut, String date_fin, String typeconge,User user) {

        this.date_debut = date_debut;
        this.date_fin = date_fin;
        this.typeconge = typeconge;
        this.user = user;
    }





    public String getDate_debut() {
        return date_debut;
    }

    public void setDate_debut(String date_debut) {
        this.date_debut = date_debut;
    }

    public String getDate_fin() {
        return date_fin;
    }

    public void setDate_fin(String date_fin) {
        this.date_fin = date_fin;
    }

    public String getTypeconge() {
        return typeconge;
    }

    public void setTypeconge(String typeconge) {
        this.typeconge = typeconge;
    }

    public Boolean getNotifier() {
        return notifier;
    }

    public void setNotifier(Boolean notifier) {
        this.notifier = notifier;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}